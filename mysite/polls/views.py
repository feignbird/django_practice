from django.shortcuts import render
from django.http import HttpResponse
# Create your views here.

def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")

def roll_out(request):
    tell_content = "Hi, this string is in a variable 'tell_content', \
    It is defined in a view function 'roll_out' \
    which is taking a request object 'HttpRequest', it contains the metadata about the request, \
    and this string is a response to the request when a certain URL is requested by the user." 
    return HttpResponse(tell_content)