from django.urls import path

from . import views


urlpatterns = [
    path('', views.index, name= 'index'),
    path('some_info/', views.roll_out, name= 'roll_out')
]

